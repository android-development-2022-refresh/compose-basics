---
title: Writing Composable Functions
template: main-full.html
---

Let's write some composable functions and learn more about them. We'll do a full walkthrough for the Movie example in a later module.

## Where to Start

Jetpack Compose defines an extension function `ComponentActivity.setContent`. This is your starting point for declaring your user interface.

A typical Jetpack Compose activity looks like

```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DeleteMe5Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(
    name: String,
) {
    Text(text = "Hello $name!")
}
```

The `onCreate` function is called when the Android framework is ready for us to declare the user interface. In the old view-based framework, `onCreate` would be used to read an XML file describing the user interface and instantiate the classes needed to run that UI. For Compose, it's much simpler; we just write functions.

What does the above do?

   * `onCreate` calls `setContent` passing a composable lambda that calls other composable functions to declare the user interface.
   * `DeleteMe5Theme` is a composable function that sets up the `MaterialTheme` composition local. This makes its colors and fonts available to all composable functions called from its lamba.
   * `Surface` declares the background of the UI, filling the available activity area. It calls `Greeting` to declare what the UI looks like.
   * `Greeting` is a composable function that's defined by this application, which calls `Text` to display the text "Hello Android"

Running the above results in

![Displaying "Hello Android"](screenshots/run1.png)

Not the most interesting user interface, but it's a start!

## The View Model

Static user interfaces are rarely useful; we really need dynamic data.

We've talked about the **Data Layer** exposing data, directly via one-shot functions, or using `Flows` to emit a stream of values. We need to expose them in the **User Interface Layer**.

There are many ways to do this; we'll use a **View Model**.

!!! note

    The concept of a "View Model" is the set of objects that provide data and logic needed for a user interface. Some logic is "business logic", and is necessary for any use of the data. Other logic is only necessary for specific user interface implementations. Consider a web-based application, a desktop application and a mobile application that all expose the same data. Each type of user interface needs some logic to prepare the data for use in that user interface, but they can all share business logic (in the **Data Layer** or **Domain Layer**) to manage the data itself.

    The "Android Architecture Components" (AAC) define a specific class called `ViewModel` which can be used as part of the conceptual "View Model" in our application, but has specific lifecycle tie-ins (for better or worse). We'll use an AAC `ViewModel`, but keep in mind that there are debates in the community about it, some saying we don't need it.

    The user-interface logic needs to exist _somewhere_, and until we've got better guidance, we'll stick with the AAC `ViewModel`. Please keep your eyes posted for alternative recommendations!

### Configuration Changes

One of the big "gotchas" in Android application development is a "configuration change". The current configuration of a device includes

   * Device orientation
   * Language and region
   * Screen width and height
   * Android API level
   * Screen density
   * Day/Night mode
   * UI Mode (phone, car, desktop, tv etc)

Some aspects of the configuration can change, such as the user selecting a different language, or, more commonly, rotating the device.

When an Activity is started, any resources it references are selected based on the current configuration. Strings are a good example here. If you define your user-visible strings in

```
src/
   main/
      res/
         values/
            strings.xml
```

and reference the string in your Activity (we'll do that using Compose's `stringResource` function), it's pulled from the best-matching `values` directory. The above directory structure represents where the **default** string values reside. By adding a suffix on the `values` directory, we can set up more-specific overrides. For example:

```
src/
   main/
      res/
         values/
            strings.xml
         values-land/
            strings.xml
         values-es/
            strings.xml
         values-fr-rFR/
            strings.xml
         values-fr-rCA/
            strings.xml
```

The `values-es` directory will take precedence when the language is Spanish. The `values-fr-rFR` and `values-FR-rCA` will take precedence when the language is French ahd the region is France or Canada.

Other resource directories, such as `drawable` might be modified based on available screen space: `drawable-w720dp` would take precedence over `drawable` when there are at least 720 density-independent pixels available in the current screen width.

Users can change some aspects of a configuration while an application is running, and we may need to refresh the user interface in response. To make this easy, Android (by default) destroys the Activity instance and recreates it, choosing the new best-batch resources when requested.

Because the Activity is destroyed, _any data residing inside it is lost_ unless you do something specific to save that data. Some data is persistent (saved in the **Data Layer**) and can easily be re-fetched, but other data, such as "which `Person` is displayed on the screen, may only needed while the application is running.

### Keeping Data via ViewModel

The AAC `ViewModel` stays alive as long as any instance of the Activity is alive, and can hold any data you want to survive a configuration change. During a configuration change, the new instance of the Activity is created _before_ the old instance is destroyed. The same `ViewModel` instance is supplied to the new Activity, making the existing data available to it.

Remember how our `ViewModel` fits into our architecture?

=== "Without Domain Layer"

    ```mermaid
    flowchart LR
        subgraph User Interface Layer
        ui[User Interface] --> vm[View Model]
        end
        vm --> repo
        ds1 --> db[(Database)]
        ds2 --> file(((File)))
        ds3 --> ws(((Web Service)))
        subgraph Data Layer
        direction LR
        repo[Repository] --> ds1[Data Source 1]
        repo[Repository] --> ds2[Data Source 2]
        repo[Repository] --> ds3[Data Source 3]
        end
    ```

=== "With Domain Layer"

    ```mermaid
    flowchart LR
        subgraph User Interface Layer
        ui[User Interface] --> vm[View Model]
        end
        vm --> uc1
        vm --> uc2
        vm --> uc3
        vm --> data
        uc1 --> repo
        uc2 --> repo
        uc3 --> repo
        data --> repo
        ds1 --> db[(Database)]
        ds2 --> file(((File)))
        ds3 --> ws(((Web Service)))
        subgraph Domain Layer
        direction LR
        data[Data]
        uc1[Use Case 1]
        uc2[Use Case 2]
        uc3[Use Case 3]
        end
        subgraph Data Layer
        direction LR
        repo[Repository] --> ds1[Data Source 1]
        repo[Repository] --> ds2[Data Source 2]
        repo[Repository] --> ds3[Data Source 3]
        end
    ```

Let's define a `ViewModel` and use it in a composable function.

!!! important

    Only the _top level_ of composable functions should touch the view model! All other composable functions should be passed data that was made available in the view model.

    The top-level composable will get data from thew view model and call view-model functions in response to events passed up from called composable functions.

## Sample View Model

Suppose we have defined a `PersonRepository`:

```kotlin
interface PersonRepository {
    val peopleFlow: Flow<List<PersonDto>>
    suspend fun getPerson(id: String): PersonDto
    suspend fun insert(vararg people: PersonDto)
    suspend fun update(vararg people: PersonDto)
    suspend fun delete(vararg people: PersonDto)
}
```

A simple view model using this might look like

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    private val repository = 
        PersonDatabaseRepository(application)

    val peopleFlow = repository.peopleFlow
    
    suspend fun getPerson(id: String) =
        repository.getPerson(id)

    suspend fun insert(vararg people: PersonDto) = 
        repository.insert(*people)
    suspend fun update(vararg people: PersonDto) = 
        repository.update(*people)
    suspend fun delete(vararg people: PersonDto) = 
        repository.delete(*people)
}
```

This view model only acts as a handle for the repository and passes-through all functions. But most view models will add more functionality (we'll see some later).

