---
title: Calling View Model Actions
template: main-full.html
---

Let's add a `Button` that deletes the first `Person` in the list.

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    ...
    suspend fun delete(vararg people: PersonDto) = 
        repository.delete(*people)
}


@Composable
fun PeopleListScreen(
    viewModel: PersonViewModel = viewModel()
) {
    val people by
        viewModel.peopleFlow.collectAsState(
            initial = emptyList()
        )

    Column {
        ...
        Button(
            onClick = {
                viewModel.delete(people[0])
            }
        ) {
            Text(text = "Delete First Person")
        }
    }
}
```

`Button` doesn't have its own text; it's just a container for another composable. Here we add a `Text` to it, but we could just have easily added an icon or drawing or a more complex composition.

When the user clicks the `Button`, the function passed for `onClick` is called. But we have a problem here; `PersonViewModel.delete()` is a `suspend` function - it can only be called from a coroutine or other `suspend` function.

We have a choice to make. We can launch a coroutine from the UI, or have the view model launch the coroutine. 

If we launch from the UI, the coroutine will be canceled when that part of the UI is removed from the composition tree. Not ideal in this case, we'd cancel if the user rotates the screen while the coroutine is running.

So we'll modify our view model to launch coroutines that last as long as the view model is alive.

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    ...

    fun insert(vararg people: PersonDto) = viewModelScope.launch {
        repository.insert(*people)
    }
    fun update(vararg people: PersonDto) = viewModelScope.launch {
        repository.update(*people)
    }
    fun delete(vararg people: PersonDto) = viewModelScope.launch {
        repository.delete(*people)
    }
}
```

!!! note

    We're only doing this for the "action" functions in the view model - requests from the user interface to make a change. Later we'll see how the `getPerson` function works.

Now, our code can delete the first item in the list!