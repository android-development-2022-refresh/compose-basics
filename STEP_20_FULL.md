---
title: Collecting Flows
template: main-full.html
---

We defined the following view model in the previous section:

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    private val repository = 
        PersonDatabaseRepository(application)

    val peopleFlow = repository.peopleFlow
    
    suspend fun getPerson(id: String) =
        repository.getPerson(id)

    suspend fun insert(vararg people: PersonDto) = 
        repository.insert(*people)
    suspend fun update(vararg people: PersonDto) = 
        repository.update(*people)
    suspend fun delete(vararg people: PersonDto) = 
        repository.delete(*people)
}
```

Let's define some composable functions that display a list of people, that we expose as a `Flow`.

```kotlin
// only reference `viewModel()` from top-level composable function(s) 
@Composable
fun PeopleListScreen(
    viewModel: PersonViewModel = viewModel()        // AAA
) {
    val people by                                   // BBB
        viewModel.peopleFlow.collectAsState(
            initial = emptyList()
        )
    
    Column {                                        // CCC
        Text(                                       // DDD
            text = "People",
            style = MaterialTheme.typography.h4,
            modifier = Modifier.padding(8.dp)
        )
        PeopleList(                                 // EEE
            people = people,
            modifier = Modifier.padding(8.dp)
        )
    }
}

@Composable
fun PeopleList(
    people: List<PersonDto>,
    modifier: Modifier = Modifier,                  // FFF
) {
    Column(modifier = modifier) {                   // GGG
        people.forEach {                            // HHH
            Text(                                   // III
                text = it.name,
                modifier = Modifier.padding(8.dp)
            )
        }
    }
}
```

!!! note

    The `viewModel()` function used in `PeopleListScreen` requires the `androidx.lifecycle:lifecycle-viewmodel-compose` dependency. We'll see that when we walk through the Movie example in detail.

| Line | Description |
|------|-------------|
| AAA  | The `viewModel` function creates a new `PersonViewModel` instance (it infers the type from the parameter definition), or fetches an existing one. This view model instance stays alive as long as we have an instance of the activity hosting the composition. |
| BBB  | For now, think of this as "launch a coroutine to fetch each emitted list of people, and expose it via the local `people` property". We'll go through it in gory detail shortly... |
| CCC  | Declare a vertical column in our user interface that hosts other composable nodes. |
| DDD  | Add a label to the top of the column. We're using the typography defined by the theme, and tweaking the layout of the `Text` using a padding `Modifier`. |
| EEE  | Call our `PeopleList` composable function. Note that we pass `people`, and _do not_ pass the view model. Most of our composable functions shouldn't know that the view model even exists. |
| FFF  | Note that we're passing in a `Modifier` so the _caller_ can adjust our layout (or other aspects). Composables should allow a `Modifier` to be passed and default it to an empty `Modifier` or other reasonable default. The `Modifier` parameter should be either the last parameter or second-to-last if the last parameter is a lambda that describes enclosed content. |
| GGG  | We use that passed-in `Modifier` in our `Column` definition. This allows the caller to control things like padding that affect the "overall" composition that this function is emitting.  |
| HHH  | We walk through the list of people using a standard `forEach` function. It's an `inline` function so its body can call other composable functions |
| III  | Emit `Text` for each `Person's` name |

!!! note

    Big note here - `List` is _not_ **Stable** as far as Compose can see. Using `List` here doesn't allow Compose to skip `PeopleList` or any composable functions called within it.

    But this may not be an issue. If you notice jank or other problems, take a closer look here and either replace the `List` with an actual immutable list implementation, or define a wrapper class that declares the list immutable by adding an @Immutable annotation (assuming nothing modifies the list or its items).

## Accessing Flows

Let's dig into that `people` definition.

```kotlin
val people by
    viewModel.peopleFlow.collectAsState(
        initial = emptyList()
    )
```

We talked about Kotlin's `by` before - it delegates the `get` and `set` for a property to another object. In this case, we defined `people` as a `val`, so it _only_ has a `get` to delegate.

First, what does `collectAsState` do?

```kotlin
@Composable
fun <T : R, R> Flow<T>.collectAsState(
    initial: R,
    context: CoroutineContext = EmptyCoroutineContext
): State<R> = produceState(initial, this, context) {
    if (context == EmptyCoroutineContext) {
        collect { value = it }
    } else withContext(context) {
        collect { value = it }
    }
}
```

It calls `produceState` to create a Compose `MutableState` instance, and then sets the `value` of that state to each emitted item from the `Flow`.

Easy for me to say, but let's see what that really means by looking at `produceState`:

```kotlin
@Composable
fun <T> produceState(
    initialValue: T,
    key1: Any?,
    key2: Any?,
    @BuilderInference producer: suspend ProduceStateScope<T>.() -> Unit
): State<T> {
    val result = remember { mutableStateOf(initialValue) }
    LaunchedEffect(key1, key2) {
        ProduceStateScopeImpl(result, coroutineContext).producer()
    }
    return result
}
```

Jetpack Compose `State` are objects that Compose can observe. When the value of `State` changes, Compose will trigger recomposition of any composable functions/lambdas that read the value.

`State` has a read-only `value` property; `MutableState` extends that by making the state writable.

Think of `State` as an observable bucket that holds a value.

The idea is that we'll launch a coroutine that passes the bucket to a `producer` function, who will drop new values into it.

But we need to store that bucket (the `MutableState`) somewhere so it lives across composition/recomposition. Because we're in a composable function, we have access to the composition tree, so we'll store the `MutableState` _inside_ the tree. The `remember` function does that for us.

So

```kotlin
val result = remember { mutableStateOf(initialValue) }
```

does

   * Create our bucket by calling `mutableStateOf`, and store the initial value in it. (We need an initial value so something is available while we're waiting for the `producer` to send something)
   * Store the bucket in the tree by calling `remember`
   * Hold the bucket locally in `result`

Next, we need to fill the bucket. `LaunchedEffect` launches a coroutine, which will be canceled and restarted if its keys change. (In this example, the keys are the initial value and the flow itself.)

!!! note

    `LaunchedEffect` is a **controlled side effect** in Compose. It allows you to specify the data that triggers its cancellation and restart. One of its common uses is to fetch data.

    Make sure that any fetches like this are done from a `LaunchedEffect` and _not_ from the body of the composable function. The body of the function _declares_ what will be done, and if you add side-effect code outside of a safe handler such as `LaunchedEffect`, that code will be run on _every_ composition/recomposition, not just when the related data (like `id` here) changes.

The launched coroutine calls `producer` passing it a _receiver_ that holds the bucket. The `producer` can then deposit items in the bucket whenever it likes.

That's `produceState`. Let's go back up a level to `collectAsState`.

```kotlin
@Composable
fun <T : R, R> Flow<T>.collectAsState(
    initial: R,
    context: CoroutineContext = EmptyCoroutineContext
): State<R> = produceState(initial, this, context) {
    if (context == EmptyCoroutineContext) {
        collect { value = it }
    } else withContext(context) {
        collect { value = it }
    }
}
```

The `producer` is the lambda

```kotlin
{
    if (context == EmptyCoroutineContext) {
        collect { value = it }
    } else withContext(context) {
        collect { value = it }
    }
}
```

This lambda switches dispatchers if necessary, and calls `collect` on the `Flow`, which will suspend until the next value is available. Once it has a value, it drops it in the bucket.

Back up one more level, where we have

```kotlin
val people by
    viewModel.peopleFlow.collectAsState(
        initial = emptyList()
    )
```

`collectAsState` returns that bucket, which can act as a Kotlin property delegate. We forward all calls to `get` the `people` value _to the bucket_. When anything else in the composable function asks for `people`, it gets the value from that bucket.

When a new value is dropped in the bucket, compose starts recomposition (which will then read the value from the bucket via the `people` local val).

