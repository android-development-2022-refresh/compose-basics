---
title: View Model Buckets
template: main-full.html
---

Sometimes you just want a "bucket" to hold a value in the view model. Using `State` in the view model makes it easy to hold values that you're not persisting in a database, and let Compose know when those values change.

For example, if we wanted to keep track of which people in a list are "selected" (so we can delete them), we could add state to the view model:

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    ...
    var selectedPeople by mutableStateOf(emptySet<String>())
}

```

We create our bucket using `mutableStateOf`, initially holding an empty set of Strings. (We'll add and remove `ids` from this set).

We expose this to the UI as `selectedPeople`. Whenever the UI wants to `get` the value, we delegate that `get` to the bucket and give the current value. 

But I'm really not keen on allowing the user interface to directly set the value of `selectedPeople`. Because it's a `var`, they have access to set it. So I change this to

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    ...
    var selectedPeople by mutableStateOf(emptySet<String>())
        private set

    fun togglePersonSelection(id: String) {
        selectedPeople =
            if (id in selectedPeople) {
                selectedPeople - id
            } else {
                selectedPeople - id
            }
    }
}

```

which makes the `set` accessible _only_ inside the view model, and exposes a `togglePersonSelection` function we can use in the UI.

!!! info

    In case you're wondering, the `in` keyword is an operator that calls `contains` on the collection. It's one of _very_ few operators I'd recommend overloading. (Take my Kotlin class to hear about the others!)

Because we're using Compose `State`, it'll note the `gets` and when a new value is dropped in the bucket, it'll trigger recomposition of composables that did those `gets`. For example:

```kotlin
@Composable
fun PeopleListScreen(
    viewModel: PersonViewModel = viewModel()
) {
    Column {
        ...
        PeopleList(
            people = people,
            selectedPeople = viewModel.selectedPeople,
            modifier = Modifier.padding(8.dp)
        )
    }
}

@Composable
fun PeopleList(
    people: List<PersonDto>,
    selectedPeople: Set<String>,
    modifier: Modifier = Modifier,
) {
    ...
}
```

Here we pass the set of `ids` to `PeopleList` and can use it to highlight which people have been selected.

So how do we handle the `togglePersonSelection` call?

First, we add a function parameter to our `PeopleList`

```kotlin
@Composable
fun PeopleList(
    people: List<PersonDto>,
    selectedPeople: Set<String>,
    onTogglePerson: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    ...
}
```

We call it `onTogglePerson` rather than `onPersonToggled` to represent that calling it is _performing_ the event, not reacting to something that has already happened. We use it inside the function when the user performs an interaction that we consider "toggling the selection".

Back in `PeopleListScreen`, we pass a function for `onTogglePerson`:

```kotlin
PeopleList(
    people = people,
    selectedPeople = viewModel.selectedPeople,
    onTogglePerson = { viewModel.togglePersonSelection(it) },
    modifier = Modifier.padding(8.dp)
)
```

This lambda takes the parameter passed (called `it` because we haven't explicitly named it) and calls `togglePersonSelection` in the view model.

Note that the parameter and return types of `onTogglePerson` and `togglePersonSelection` are the same: 

```kotlin
(String) -> Unit
```

Both take a `String` parameter and neither returns a value. Because of this, we can use a Kotlin **function reference** instead:

```kotlin
PeopleList(
    people = people,
    selectedPeople = viewModel.selectedPeople,
    onTogglePerson = viewModel::togglePersonSelection,
    modifier = Modifier.padding(8.dp)
)
```

which eliminates the intermediary lambda. Note that you never *have* to use a function reference, but they're a wee bit shorter and possibly a weeeeeeeeee bit faster.

!!! note

    We'll go into the actual select/deselect process in much more detail when we talk about lists in a later module.