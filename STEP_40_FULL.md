---
title: Calling View Model Accessors
template: main-full.html
---

View models can also have functions that help the user interface access data.

Think about "screens" in your application, composable functions that define the entire visible user interface that the user sees at one time.

We might use a **Sealed Interface** to describe the available screens. If we wanted to define two screens, a list of people and the details of one person, we might define:

```kotlin
sealed interface Screen
object PeopleListScreen: Screen
data class PersonDisplay(
    val id: String
): Screen
```

Subtypes of a `sealed interface` can _only_ be defined in the same **module** as the `sealed interface`. This allows the Kotlin compiler (which compiles all code in a module together), to know _all_ of its possible subtypes, which in turn, allows you to write exhaustive `when` expressions without an `else`.

We define `PeopleListScreen` as an `object` because there's no data needed to know what to display; we always display the entire list of people.

`PersonDisplay`, on the other hand, needs to know _which_ `Person` we want to display. And now we need to think about how to represent that...

We _could_ just drop a `Person` object into it. When we call a composable function to generate its display, we just read the data from that object.

However... the `Person` could _change_. Think about an application where the user can traverse from displaying one `Person` to another. We'd store the `Screens` on a stack so the user can go back to previous screens.

If a `Person` that happened to be held as data for a previous screen changes, going back would see **stale data**.

So... we choose to store the `id` of the `Person`. That won't change. And now we need to enable our composable function to fetch the person by its `id`.

That's where our `getPerson` function comes in:

```kotlin
class PersonViewModel(
    application: Application
) : AndroidViewModel(application) {
    ...
    suspend fun getPerson(id: String) = repository.getPerson(id)
}
```

We need to pass an `id` to our composable function and have it fetch the `Person`. To do this, we follow a similar technique we saw when we examined `collectAsState`:

```kotlin
@Composable
fun PersonDisplay(
    id: String,
    fetchPerson: suspend (String) -> PersonDto,
) {
    var person by remember {                // AAA
        mutableStateOf<PersonDto?>(null)
    }
    
    LaunchedEffect(key1 = id) {             // BBB
        person = fetchPerson(id)
    }
    
    // if we want to only display fields when we have a person
    person?.let {                           // CCC
        // display the Person
    }
    
    // if we want to display temporary values when we don't 
    // have a person
    val name = person?.name ?: "(loading)"  // DDD
        // Elvis operator: left side if non-null, else right side 
    Text(text = "Name: $name")
}
```

| Line | Description |
|------|-------------|
| AAA  | Create a bucket to hold a nullable `PersonDto`. Store it in the composition tree. Delegate `get` and `set` of `person` to the bucket. If Compose sees a new value in the bucket, it'll start a recomposition. |
| BBB  | Start a coroutine to fetch the person by its `id`. When received, store it in `person`. If we see a different `id`, cancel the coroutine (if still running) and launch again. |
| CCC  | If you want to omit fields entirely if we don't yet have a `person`, use `let` (or another scoping function) to contain the UI only if `person` is not null. |
| DDD  | If you want to show fields with temporary values, use the **Elvis operator** `?:` to choose the non-null value or a default. Turn your head 90 degrees to the left to see how it looks like Elvis. If you don't know who Elvis is, you're really making me feel old. But that's ok. I'm not a fan anyway... |
