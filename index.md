---
title: Compose Basics
template: main-full.html
---

Now that we have a **Data Layer** (using Android Room and a Repository), let's focus on our **User Interface layer**.

We'll be using Jetpack Compose to implement our user interface, but before we start talking about the UI, what is Jetpack Compose?

There are two general parts that make up Compose:

   * Runtime - general-purpose framework for creating trees and managing state
   * UI - user-interface components in the form of **Composable Functions**, that emit tree nodes

The runtime can do much more than manage UI trees, and I look forward to it being used for other tree management as developers discover this. (Currently these two parts are usually thought of as a "UI framework", but hopefully Google will refactor them into separate dependencies to encourage the runtime use in other contexts).

!!! note

    If you're interested in an example of the runtime being used outside Compose UI, take a look at [https://arunkumar.dev/jetpack-compose-for-non-ui-tree-construction-and-code-generation/](https://arunkumar.dev/jetpack-compose-for-non-ui-tree-construction-and-code-generation/), exploring code-generation using Compose runtime.

## Kotlin-Only

Jetpack Compose uses some advanced features of Kotlin, including its compiler-plugin support. Because of this, you can only use Jetpack Compose inside a Kotlin compilation unit (file, class, interface, etc)

## Declaring a Tree

Jetpack Compose is a **declarative** framework. Chances are, most of the programming you've done is **imperative**. You've written code that describes **how** to do something, as a series of steps and calls to other code that describes **how** to do something. 

**Imperative** programming is quite useful, but requires that you specify *how* to do a task, which tends to be verbose and forces you to define abstractions when you want to hide that *how*. A typical imperative user interface consists of calls to functions that present a UI on the screen.

**Declarative** programming means you specify *what* you want to do or what you want your output to look like. You may have heard of **Domain-Specific Langauges** (DSLs). These are little languages that are often used to describe *what* to do.

For example, using a tool like ANTLR [https://www.antlr.org/](https://www.antlr.org/), you define a *grammar* that *describes* what a language looks like, with embedded actions to perform work when you see certain constructs. Tools like these are often used to create compilers (translating code in that language to lower-level code), interpreters (running code in that the language directly) or static analyzers (examining code in that language to produce reports). 

A **declarative** user-interface consists of calls that describe the user interface, and other parts of the UI framework render it.

That may not seem like a big distinction, but it allows for some great optimization (and potentially parallel processing) when rendering.

We declare a tree that represents the UI by defining **Composable Functions**. Annotating a function with `@Composable` declares it will be *emitting* nodes to a tree or defining data for use during composition. The Compose compiler plugin tweaks the definition of the function to add a `Composer` parameter, which has an `emit` function that adds to the tree. Some lower-level composable functions will emit nodes; some functions will simply call other composable functions to build up more complex structures.

When you define your composable functions, you probably won't be emitting nodes directly. Rather, you'll declare your user interface using existing components declared using other composable functions.

!!! note 

    You'll probably hear at some point that under the covers, the tree that represents the UI is implemented using a **Gap Buffer* data structure known as the "slot table", which is an Array with a moveable empty chunk of nodes that optimizes updates and reads. If you want to really dig deep into the internals, you may want to read [https://medium.com/@takahirom/inside-jetpack-compose-2e971675e55e](https://medium.com/@takahirom/inside-jetpack-compose-2e971675e55e) or [https://jorgecastillo.dev/book/](https://jorgecastillo.dev/book/). 

    You don't **need** to know this level of detail, but there are some pretty cool things going on behind the scenes.

It's important to think of your Composable functions as **declaring** your user interface, as much happens between the time you declare it and its realization on the screen.

!!! note

    We've been talking about Compose emitting nodes to "a tree". Compose actually keeps track of **two** trees:

       * Composition Tree - the tree that contains the description of your user interface
       * Semantics Tree - a parallel tree that adds metadata about the composition. This metadata is typically
          * Accessibility data and actions
          * Test data such as labels to access nodes in the composition tree

    We'll look into the Semantics Tree in more detail when we discuss testing our applications.


## Composition vs Recomposition

Your composable functions will be called a *lot*, potentially for every frame displayed to the user. 

!!! info 

    "Frame" is a term used in computer animation, describing a unique image shown to the user. During an animation, multiple frames are displayed to the user at a rate that tricks the eye into thinking things are moving on the screen. The "frame-rate" is measured in "frames-per-second" or more generally, "hertz". Hertz is a unit of *frequency*, where 1hz is one cycle per second. The higher the frequency, the faster the animation and less the eye can detect the switching between images. Faster animation appears smoother. 
    
    If anything delays a new image from being displayed on time, the user experiences "jank", a visual pause in the animation, which can be very distracting. Think about watching a show on a streaming service, and suddenly the image freezes due to network slowness and then restarts. That's an extreme case of jankiness, but the human eye can detect much smaller pauses during a smooth animation.

**Composition** is the first time a composable function is called. Your function will declare the entire visible user interface and return. 

**Recomposition** is later calls to the same function when the data passed has changed. Jetpack Compose uses a **Snapshot System** to track data that's been previously passed to functions, and compares the last values to current values.

Let's take a look at an example:

```kotlin
@Composable
fun MyScreen(
    count: Int,
    name: String,
) {
    Column {
        TopPart(count = count)
        BottomPart(name = name)
    }
}

@Composable
fun TopPart(
    count: Int,
) {
    ...
}

@Composable
fun BottomPart(
    name: String,
) {
    ...
}
```

Suppose in our initial composition, we call 

```kotlin
MyScreen(viewModel.count, viewModel.name)
```

(assuming `viewModel.count` and `viewModel.name` are state that is observable by Jetpack Compose - we'll describe that in more detail later).

During initial composition, count is 1 and name is "Scott". Compose's snapshot system remembers this. The UI tree is emitted and rendered.

Later, the user does something that changes the name, but not the count. Compose detects the change to name, and asks `MyScreen` and `BottomPart` to update their parts of the tree. The tree changes are detected by compose and trigger updates to those parts of the screen.

Note that `TopPart` isn't asked to recompose. How can this be?

## Unidirectional Data Flow

Composable functions take two kinds of parameters:

   * Data - the state that drives what the user will see
   * Event functions - functional parameters that inform the caller when something happens

Data is passed down, from composable function to other composable functions. Composable functions do not access any data outside of them *unless that access is controlled* (more on that later).

If a composable function needs to modify data, it does not *directly* perform the update; it calls one of its function parameters to inform the caller of the update. This ensures that the part of your application that manages its state is involved in all updates, keeping its state consistent.

Data in; function calls out. For example:

```kotlin
@Composable
fun MyButton(
    text: String,
    onClick: () -> Unit,
) {
    ...
}
```

We pass `text` in to be displayed on the button, and call `onClick` when the user clicks the button.

## Composition Locals

!!! caution

    I strongly advise against creating your own composition locals unless you have a **very** good reason to do so. They make composable functions more difficult to reason about and more difficult to test.

Compose supports implicitly-available scoped data objects called "Composition Locals". These objects are scoped at some level of the tree and available to all composable functions nested within that level, without passing them as parameters.

There are some pre-defined composition locals, including

   * MaterialTheme - gives access to colors, fonts, etc
   * LocalDensity - describes the screen density and provides functions for converting between density-independent pixels and screen pixels
   * LocalContext - access to the current Android context (usually an Activity) as a handle to resources and platform features

You access them by making a call like

```kotlin
@Composable
fun SomeComposable(
    padding: Dp,
) {
    with (LocalDensity.current) {
        val paddingInPixels = padding.toPx()

        // use graphics-drawing features that require pixels
    }
}
```

You can define your own composition locals, but you generally shouldn't. You may think they're convenient for making values available down through a long call chain, but that makes the functions more difficult to reason about and test. 

See [https://developer.android.com/jetpack/compose/compositionlocal](https://developer.android.com/jetpack/compose/compositionlocal) for a more detailed discussion if you're thinking about creating one.

## Idempotency and Stable Data

Composable functions use data that's passed into them to determine what to emit. _If a composable function is called with the same data parameters, it should emit the same nodes to the tree._ This concept is called **idempotency**, and is key to Compose's optimization. If a function is **idempotent**, Compose can assume that if the parameters haven't changed, it doesn't need to refresh that part of the UI.

But Compose can only do this if it knows the data coming in is **stable**.

**Stable** data is data that Compose can track for changes. Think about a `Person` class like

```kotlin
data class Person(
    var name: String,
    var age: Int,
)
```

If we pass in an instance of `Person` to a Composable function, there's no way for Compose to know when the `name` or `age` change; it cannot know when to perform updates. It's also possible for the function to update the `Person` directly and nothing outside the function would know.

And even more dangerously - if another thread updates that `Person` at the same time the tree nodes are being emitted, the displayed data could be inconsistent. For example:

   * We pass in `Person("Scott", 55)` to a composable function
   * The function creates a `Text` node to display `"Scott"`
   * Another thread updates the `name` and `age` of the `Person` to `"Mikey"` and `10`.
   * The function creates a `Text` node to display `10`. The displayed data is inconsistent.

**Stable** data is usually made up of Jetpack Compose `State` objects. Whenever `State` is read, the Compose snapshot system takes a note of the read at the current point of the composition, and watches it for changes. If the `State` changes, compose starts recomposition and knows exactly which parts of the tree to re-emit.

In the above example, Compose would know that the `name` and `age` have changed and would recompose again.

For a class to be **Stable**, all properties of that class must be **Stable** as well.

**Immutable** data is the lowest-level **Stable** data in a passed-in data structure. **Immutable** data is data that cannot be changed (and must be _deeply_ immutable - all of its properties are immutable all the way down). We'll talk about Stable `State` in more detail later, but as a quick example:

```kotlin
class Person(
    initialName: String = "",
    initialAge: Int = 0,
) {
    var name by mutableStateOf<String>(initialName)
    var age by mutableStateOf<Int>(initialAge)
    var bestFriend by mutableStateOf<Person?>(null)
}
```

There's a lot to unpack here:

   * You can pass in a name and age when creating a `Person`, or let them default to `""` and `0` respectively.
   * `name` is managed by a Compose `MutableState` object. Whenever it is read or updated, the Compose snapshot system takes note or starts recomposition (similar for `age` and `bestFriend`). 
   * The `by` keyword in Kotlin *delegates* a property to an object that manages its data. Looking at `name`:
      * The `name` property has no backing field, so it cannot store its own data.
      * Whenever `name` is read, Kotlin asks the *delegate* object (the `MutableState`) to get the value. 
      * Whenever `name` is changed, Kotlin asks the *delegate* to set the value.
      * This is similar to writing 
        ```kotlin
            private var nameState = mutableStateOf<String>(initialName)
            var name: String
                get() = nameState.value
                set(value) {
                    nameState.value = value
                }

        ```
      * The `MutableState` instance has `get` and `set` functions that interact with the Snapshot system. That's what makes the magic happen!
   * The values of `name` and `age` are **Immutable**. The value of `bestFriend` is **Stable**; Compose can listen to its properties for changes.

Data that is not **stable** can be passed to a Composable function, but the function won't be able to skip calls that use that data.

For a great deep-dive into stability, see [https://medium.com/androiddevelopers/jetpack-compose-stability-explained-79c10db270c8](https://medium.com/androiddevelopers/jetpack-compose-stability-explained-79c10db270c8).


## Uncontrolled Side Effects

Uncontrolled side effects are data access or changes that happen outside of a composable function.

Suppose we had the following:

```kotlin
// DO NOT DO THIS!!!
var count = 1

@Composable
fun DoNotDoThis(
    text: String,
    onClick: () -> Unit,
) {
    count++
    Text(text = count.toString())
    ...
}
```

This function is **not** idempotent. Each time you call it with the same parameters, you'll get a different count printed, and that `count` variable is updated whenever the the function is recomposed.

Later we'll see that we can use **controlled** side effects to perform processing that compose is aware of.

## Calling Composable Functions

Composable functions can only be called from other composable functions, or from Kotlin `inline` functions.

!!! note

    Most collection/stream functions are `inline`. This means that the call is replaced by the body of the function. You can use functions like these to call other composable functions. For example:

    ```kotlin
    @Composable
    fun ShowList(
        list: List<String>,
    ) {
        list.forEach { item ->
            Text(text = item)
        }
    }

    ```

